package com.cisco.cmad.so.service;

import com.cisco.cmad.so.util.MongoUtil;
import com.mongodb.MongoWriteException;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTOptions;
import io.vertx.ext.mongo.FindOptions;

public class UsersService {

    private static final Logger logger = LoggerFactory.getLogger(UsersService.class.getName());

    public static final String USERS = "users";
    public static final String USER_ID = "userName";

    private JWTAuth provider;

    public void setProvider(JWTAuth provider) {
        this.provider = provider;
    }

    public void createUser(Message<Object> message) {

        JsonObject document = new JsonObject(message.body().toString());
        logger.debug("Creating a new user." + document.toString());

        // Note, there is an index on userName which ensures the attribute is
        // unique.

        // Insert DB Entry
        MongoUtil.getMongoClient().insert(USERS, document, reply -> {
            if (reply.succeeded()) {
                message.reply(reply.result());
            } else {
                logger.error("Error Creating user in Database", reply.cause());

                if (reply.cause() != null && reply.cause() instanceof MongoWriteException) {
                    int errorCode = ((MongoWriteException) reply.cause()).getError().getCode();
                    if (errorCode == 11000) {
                        logger.error("Unique key violation for userName while adding user : "
                                + document.getString("userName"));
                        message.fail(409, "UserName (Email) already exists.");
                        return;
                    }
                }
                message.fail(500, "Database error.");
            }
        });
    }

    public void searchUsers(Message<Object> message) {
        System.out.println("Search users from database.");
        logger.debug("Search users from database.");
        JsonObject document = new JsonObject(message.body().toString());

        int skip = document.getInteger("skip");
        int limit = document.getInteger("limit");
        String text = document.getString("text");

        FindOptions findOptions = new FindOptions();
        // Apply skip and limit.
        findOptions.setLimit(limit);
        findOptions.setSkip(skip);

        JsonObject query = new JsonObject();
        JsonObject sort = new JsonObject();
        JsonObject fields = new JsonObject();

        // Do not retrieve password.
        fields.put("password", 0);

        // Search by question text.
        // If no text is given, get all questions
        if (text != null && text.trim().length() > 0) {
            JsonObject subQuery = new JsonObject();
            subQuery.put("$search", text);
            query.put("$text", subQuery);

            // Include search score metadata in the result
            JsonObject metaField = new JsonObject();
            metaField.put("$meta", "textScore");
            fields.put("searchScore", metaField);

            // Sort by relevance and then by date.
            sort.put("searchScore", metaField);
        } 
        sort.put("firstName", 1);
        sort.put("lastName", 1);

        findOptions.setFields(fields);
        findOptions.setSort(sort);

        JsonObject result = new JsonObject();

        MongoUtil.getMongoClient().count(USERS, query, countReply -> {
            if (countReply.succeeded()) {
                result.put("total", countReply.result());
                MongoUtil.getMongoClient().findWithOptions(USERS, query, findOptions, searchReply -> {
                    if (searchReply.succeeded()) {
                        JsonArray resultArray = new JsonArray(searchReply.result());
                        result.put("data", resultArray);
                        message.reply(result.toString());
                    } else {
                        logger.error("Error searching users from database", searchReply.cause());
                        message.fail(500, "Database error.");
                    }
                });
            } else {
                logger.error("Error counting search users from database", countReply.cause());
                message.fail(500, "Database error.");
            }
        });
    }

    public void getUserById(Message<Object> message) {
        logger.debug("Get user by Id");
        JsonObject document = new JsonObject(message.body().toString());

        String userId = document.getString("userId");
        String field = document.getString("field");

        JsonObject query = new JsonObject();
        query.put(field, userId);

        JsonObject fields = new JsonObject();
        // Do not retrieve password.
        fields.put("password", 0);

        MongoUtil.getMongoClient().findOne(USERS, query, fields, reply -> {
            if (reply.succeeded()) {
                if (reply.result() == null)
                    message.reply(null);
                else {
                    String userEmailId = reply.result().getString("userName");
                    // Get Questions count posted by this user.
                    MongoUtil.getMongoClient().count("questions", new JsonObject().put("userId", userEmailId),
                            questionsCountHandler -> {
                                if (questionsCountHandler.succeeded()) {
                                    reply.result().put("questionsPosted", questionsCountHandler.result());
                                } else {
                                    reply.result().put("questionsPosted", 0);
                                }

                                // Get Answers count posted by this user.
                                MongoUtil.getMongoClient().count("questions",
                                        new JsonObject().put("answers.userId", userEmailId), answersCountHandler -> {
                                            if (answersCountHandler.succeeded()) {
                                                reply.result().put("answersPosted", answersCountHandler.result());
                                            } else {
                                                reply.result().put("answersPosted", 0);
                                            }

                                            // Return response
                                            message.reply(reply.result().toString());
                                        });
                            });
                }
            } else {
                logger.error("Error getting user by ID from database", reply.cause());
                message.fail(500, "Database error.");
            }
        });
    }

    public void updateUser(Message<Object> message) {
        logger.debug("Updating user...");

        JsonObject user = new JsonObject(message.body().toString());
        String userId = user.getString(USER_ID);

        JsonObject update = new JsonObject().put("$set", new JsonObject());
        for (String field : user.fieldNames()) {
            update.getJsonObject("$set").put(field, user.getValue(field));
        }

        JsonObject query = new JsonObject().put(USER_ID, userId);

        MongoUtil.getMongoClient().updateCollection(USERS, query, update, reply -> {
            if (reply.succeeded()) {
                logger.debug("Matched : " + reply.result().getDocMatched());
                logger.debug("Updated : " + reply.result().getDocModified());
                if (reply.result().getDocMatched() == 0) {
                    message.fail(404, "User does not exist.");
                } else {
                    message.reply(reply.result().toString());
                }
            } else {
                logger.error("Error in database while updating user...", reply.cause());
                message.fail(500, "Database error.");
            }
        });
    }

    public void authenticate(Message<Object> message) {

        logger.info("authenticate the user.");
        JsonObject document = new JsonObject(message.body().toString());
        String userName = document.getString("userName");
        String password = document.getString("password");
        JsonObject query = new JsonObject();
        query.put("userName", userName);

        logger.info("find the query." + query);

        MongoUtil.getMongoClient().findOne(USERS, query, null, reply -> {
            if (reply.succeeded()) {

                JsonObject dbRecord = reply.result();
                String dbPassword = dbRecord.getString("password");
                String token = "";

                if (dbPassword.equals(password)) {
                    token = generateUserToken(dbRecord);
                    dbRecord.put("jwtToken", token);
                }
                message.reply(dbRecord.toString());

            } else {
                logger.error("Error getting question by ID from database", reply.cause());
                message.fail(500, "Database error.");
            }
        });

    }

    private String generateUserToken(JsonObject dbRecord) {

        String userId = dbRecord.getString("userName");
        String firstName = dbRecord.getString("firstName");
        String lastName = dbRecord.getString("lastName");
        String entityId = dbRecord.getString("_id");
        
        JsonObject userToken = new JsonObject().put("sub", userId).put("firstName", firstName).put("id", entityId);
        if (lastName != null)
            userToken.put("lastName", lastName);

        String token = provider.generateToken(userToken, new JWTOptions());
        // logger.info("Token Generated:" + token);
        return token;
    }
}
