package com.cisco.cmad.so.util;

import com.cisco.cmad.so.service.UsersService;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.IndexOptions;
import io.vertx.ext.mongo.MongoClient;

public class MongoUtil {

    private static Logger logger = LoggerFactory.getLogger(MongoUtil.class.getName());

    public static final String ENV_MONGODB_DATABASE = "SO_MONGODB_DATABASE";

    public static final String ENV_MONGODB_URL = "SO_MONGODB_URL";

    private static final String DEFAULT_MONGODB_DATABASE = "cmad";

    private static final String DEFAULT_MONGODB_URL = "mongodb://cmad-mongodb:27017";

    private static MongoClient client = null;

    private MongoUtil() {
    }

    public static synchronized void initialize(Vertx vertx) {
        if (client != null)
            return;

        logger.info("Initializing mongodb client");

        String mongoDbName = DEFAULT_MONGODB_DATABASE;
        String mongoDbUrl = DEFAULT_MONGODB_URL;

        // Over-ride defaults from Environment variable, if available.
        String envMongoDbName = System.getenv(ENV_MONGODB_DATABASE);
        if (envMongoDbName != null && !envMongoDbName.isEmpty()) {
            logger.info(
                    "Found and using environment variable " + ENV_MONGODB_DATABASE + " with value " + envMongoDbName);
            mongoDbName = envMongoDbName;
        } else {
            logger.info("Using default mongodb database : " + DEFAULT_MONGODB_DATABASE);
        }

        String envMongoDbUrl = System.getenv(ENV_MONGODB_URL);
        if (envMongoDbUrl != null && !envMongoDbUrl.isEmpty()) {
            logger.info("Found and using environment variable " + ENV_MONGODB_URL);
            mongoDbUrl = envMongoDbUrl;
        } else {
            logger.info("Using default mongodb url : " + DEFAULT_MONGODB_URL);
        }

        JsonObject config = new JsonObject();
        config.put("db_name", mongoDbName);
        config.put("connection_string", mongoDbUrl);
        client = MongoClient.createShared(vertx, config);

        // Create indexes on the MongoDB instance.
        initializeIndices();
    }

    public static MongoClient getMongoClient() {
        return client;
    }

    public static boolean isInitialized() {
        return client != null;
    }

    private static void initializeIndices() {

        // Indexes for Users collection.
        logger.info("Initializing indices for users collection");

        // INDEX 1: Add index on first name and last to allow searching for
        // users by name.
        JsonObject searchIndex = new JsonObject();
        searchIndex.put("firstName", "text").put("lastName", "text").put("userName", "text");

        // Give email id twice as much weight as first or last name.
        IndexOptions searchIndexOptions = new IndexOptions();
        JsonObject weights = new JsonObject().put("firstName", 5).put("lastName", 5).put("userName", 10);
        searchIndexOptions.weights(weights);

        logger.info("Creating index to search users by firstName, lastName and userName");
        getMongoClient().createIndexWithOptions(UsersService.USERS, searchIndex, searchIndexOptions, result -> {
            if (result.failed()) {
                logger.error("Error creating searchIndex on users.", result.cause());
            }
        });

        // INDEX 2: Add index to ensure userName is unique.
        JsonObject userNameUniqueIndex = new JsonObject();
        userNameUniqueIndex.put("userName", 1);
        IndexOptions uniqueIndexOptions = new IndexOptions();
        uniqueIndexOptions.unique(true);

        logger.info("Creating index on userName to make sure it is unique.");
        getMongoClient().createIndexWithOptions(UsersService.USERS, userNameUniqueIndex, uniqueIndexOptions, result -> {
            if (result.failed()) {
                logger.error("Error creating userNameUniqueIndex on users.", result.cause());
            }
        });
    }

}
