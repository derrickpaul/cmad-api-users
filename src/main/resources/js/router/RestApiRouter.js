/**
 * Holds all the REST API route definitions.
 */


var RestApiRouter = {
    logger : Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.router.RestApiRouter"),
    router : null,

    create : function() {
        var Router = require("vertx-web-js/router");
        this.router = Router.router(vertx);

        RestApiRouter.logger.info("Configuring routes for all REST API.");

        load("classpath:js/handler/UsersHandler.js");

        // Users and authentication handlers
        this.router.post("/users").handler(UsersHandler.createUser);
        this.router.get("/users").handler(UsersHandler.searchUsers);
        this.router.get("/users/:userId").handler(UsersHandler.getUserById);
        this.router.patch("/users/:userId").handler(UsersHandler.updateUser);
        this.router.put("/users/:userId").handler(UsersHandler.updateUser);
        this.router.post("/tokens").handler(UsersHandler.generateToken);

        return this;
    }
}