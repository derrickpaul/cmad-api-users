/**
 * Handler to catch and handle any exception or forced failure from any of the
 * API handlers. This handler will return corresponding status code and end the
 * response.
 */
var InitializationHandler = {
    logger : Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.handler.InitializationHandler"),
    handle : function(rc) {
        // Log the request in trace mode.
        if (InitializationHandler.logger.isTraceEnabled()) {
            InitializationHandler.logger.trace("Request received: " + rc.request().method() + " " + rc.request().uri());
        }
        
        // Log all requests, just for demo purposes:
        InitializationHandler.logger.info("Request received: " + rc.request().method() + " " + rc.request().uri());
        
        // Set the response as chunked and content type.
        rc.response().setChunked(true).putHeader("content-type", "application/json")
        .putHeader("Access-Control-Allow-Origin", "*")
        .putHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, PATCH")
        .putHeader("Access-Control-Allow-Headers", "content-type, origin, accept, authorization")
        .putHeader("Access-Control-Expose-Headers", "content-type, location, authorization, transfer-encoding, content-length, cache-control")
        .putHeader("Access-Control-Max-Age", "86400");
        rc.next();
    }
}