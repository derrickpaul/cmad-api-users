/**
 * Handles operations related to user and authentication.
 */

var WebSocketHandler = {
    logger : Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.handler.WebSocketHandler"),
    handle : function(ws) {
        var success = false;
        if (ws.path().indexOf("/apis/users/") !== -1) {
            WebSocketHandler.logger.info("Received websocket request for path: " + ws.path() + " and client : " + ws.textHandlerID());
            var userName = ws.path().split("users/")[1];
            if (userName != null) {
                userName = userName.split("/chat")[0];
                if (userName != null) {
                    WebSocketHandler.handleClientOpen(ws, userName);
                    success = true;
                }
            }
        }
        if (!success) {
            WebSocketHandler.logger.error("Received websocket request for an unknown URL : " + ws.path()
                    + ". Rejecting the websocket connection.");
            ws.reject();
        }
    },
    handleClientOpen : function(ws, userName) {
        // Add a data handler to write data to the websocket.
        ws.handler(function(message) {
            WebSocketHandler.handleMessage(ws, message);
        });

        // Add a close handler
        ws.closeHandler(function(event) {
            WebSocketHandler.handleClientClose(ws, event);
        });

        // Add an exception handler.
        ws.exceptionHandler(function(cause) {
            WebSocketHandler.handleClientException(ws, cause);
        });

        // Add the clientId and userName to two shared Maps.
        WebSocketHandler.addToSharedMaps(ws.textHandlerID(), userName);

        // Broadcast user online message.
        WebSocketHandler.broadcastUserOnlineStatus(ws, userName);
    },
    handleClientClose : function(ws, event) {
        WebSocketHandler.logger.info("Closing client connection : " + ws.textHandlerID());
        // Remove entries from shared map.
        WebSocketHandler.removeFromSharedMaps(ws.textHandlerID());
    },
    handleClientException : function(ws, cause) {
        WebSocketHandler.logger.error("Exception occurred in WebSocket client : " + ws.textHandlerID(), cause);
    },
    handleMessage : function(ws, data) {
        WebSocketHandler.logger.info("Handling message : " + data + "for client " + ws.textHandlerID());
        var message = JSON.parse(data);
        if (message == null)
            return;

        if (message.fromEventBus) {
            WebSocketHandler.logger.info("Sending message to client");
            ws.writeTextMessage(data);
        } else {
            WebSocketHandler.logger.info("Received message from client");

            if (!message.fromUserName || message.fromUserName == null || message.fromUserName.trim().length == 0) {
                WebSocketHandler.sendErrorMessage(ws, {
                    type : "ERROR",
                    message : "Invalid or missing fromUserName"
                });
                return;
            }

            if (!message.toUserName || message.toUserName == null || message.toUserName.trim().length == 0) {
                WebSocketHandler.sendErrorMessage(ws, {
                    type : "ERROR",
                    message : "Invalid or missing toUserName"
                });
                return;
            }

            if (!message.type || message.type == null
                    || (message.type.toUpperCase() != "CHAT" && message.type.toUpperCase() != "USER_STATUS")) {
                WebSocketHandler.sendErrorMessage(ws, {
                    type : "ERROR",
                    message : "Invalid or missing type"
                });
                return;
            }

            if (message.type && message.type.toUpperCase() == "CHAT") {
                WebSocketHandler.sendChatMessage(ws, message);
            } else if (message.type && message.type.toUpperCase() == "USER_STATUS") {
                WebSocketHandler.findUserPresence(ws, message.toUserName);
            } else {
                WebSocketHandler.logger.error("Unknown or missing message type : " + message.type);
            }
        }
    },
    sendChatMessage : function(ws, message) {
        WebSocketHandler.getClientsFromUserMap(message.toUserName, function(clients) {
            if (!clients || clients == null || clients.length == 0) {
                WebSocketHandler.logger.info("sendChatMessage :: No clients found");
                WebSocketHandler.sendUserPresence(ws, message.toUserName, "OFFLINE");
                return;
            }

            message.fromEventBus = true;
            for (var i = 0; i < clients.length; i++) {
                WebSocketHandler.logger.info("sendChatMessage :: sending to client " + clients[i]);
                vertx.eventBus().publish(clients[i], JSON.stringify(message));
            }
        });
    },
    sendUserPresence : function(ws, userName, presence) {
        var message = {
            fromEventBus : true,
            userName : userName,
            type : "USER_STATUS",
            presence : presence
        };

        vertx.eventBus().publish(ws.textHandlerID(), JSON.stringify(message));
    },
    findUserPresence : function(ws, userName) {
        WebSocketHandler.getClientsFromSharedMap(message.toUserName, function(clients) {
            if (!clients || clients == null || clients.length == 0)
                WebSocketHandler.sendUserPresence(ws, message.toUserName, "OFFLINE");
            else
                WebSocketHandler.sendUserPresence(ws, message.toUserName, "ONLINE");
        })
    },
    sendErrorMessage : function(ws, message) {
        message.fromEventBus = true;
        vertx.eventBus().publish(ws.textHandlerID(), JSON.stringify(message));
    },
    broadcastUserOnlineStatus : function(ws, userName) {

    },
    broadcastUserOfflineStatus : function(ws, clientId) {

    },
    addToSharedMaps : function(clientId, userName) {
        var sharedData = vertx.sharedData();
        if (vertx.isClustered()) {
            // If Vertx is clustered, use cluster wide maps.
            sharedData.getClusterWideMap("clientMap", function(clientMap, error) {
                if (error) {
                    WebSocketHandler.logger.error("Error getting clustered clientMap for user : " + userName, error);
                } else {
                    // Remove data after one day.
                    clientMap.put(clientId, userName, 86400000, function(result, error) {
                        if (error) {
                            WebSocketHandler.logger.error("Error putting data to clustered clientMap for user : "
                                    + userName, error);
                        }
                    });
                }
            });

            sharedData.getClusterWideMap("userMap", function(userMap, error) {
                if (error) {
                    WebSocketHandler.logger.error("Error getting clustered userMap for user : " + userName, error);
                } else {
                    userMap.get(userName, function(clients, error) {
                        if (error) {
                            WebSocketHandler.logger.error(
                                    "Error getting client list from clustered userMap for user : " + userName, error);
                        } else {
                            if (!clients || clients == null)
                                clients = [];
                            clients.push(clientId);
                            
                            WebSocketHandler.logger.info("Adding clients to clustered userMap : " + clients);
                            // Remove data after one day.
                            userMap.put(userName, clients, 86400000, function(result, error) {
                                if (error) {
                                    WebSocketHandler.logger.error("Error putting data to clustered userMap for user : "
                                            + userName, error);
                                }
                            });
                        }
                    });
                }
            });
        } else {
            // If Vertx is not clustered, use local map.
            var clientMap = sharedData.getLocalMap("clientMap");
            clientMap.put(clientId, userName);
            WebSocketHandler.logger.info("Added to client map : " + clientId + " : " + userName);

            var userMap = sharedData.getLocalMap("userMap");
            var clients = userMap.get("userName");
            if (clients == null)
                clients = [];
            clients.push(clientId);
            userMap.put(userName, clients);
            WebSocketHandler.logger.info("Adding to user map : " + clientId + " : " + userName);
        }
    },
    removeFromSharedMaps : function(clientId) {
        var sharedData = vertx.sharedData();
        if (vertx.isClustered()) {
            // If Vertx is clustered, use cluster wide maps.
            sharedData.getClusterWideMap("clientMap", function(clientMap, error) {
                if (error) {
                    WebSocketHandler.logger.error("Error getting clustered clientMap for client : " + clientId);
                } else {
                    clientMap.get(clientId, function(userName, error) {
                        if (error) {
                            WebSocketHandler.logger.error("Error getting userName from clientMap for client : "
                                    + clientId);
                        } else {
                            // Remove data after one day.
                            clientMap.remove(clientId, function(result, error) {
                                if (error) {
                                    WebSocketHandler.logger
                                            .error("Error removing clientId from clustered clientMap for client : "
                                                    + clientId, error);
                                }
                            });

                            // Remove ClientId from userMap.
                            sharedData.getClusterWideMap("userMap", function(userMap, error) {
                                if (error) {
                                    WebSocketHandler.logger.error("Error getting clustered userMap for user : "
                                            + userName, error);
                                } else {
                                    userMap.get(userName, function(clients, error) {
                                        if (error) {
                                            WebSocketHandler.logger.error(
                                                    "Error getting client list from clustered userMap for user : "
                                                            + userName, error);
                                        } else {
                                            if (!clients || clients == null || clients.length == 0)
                                                return;

                                            clients.splice(clients.indexOf(clientId), 1);
                                            // Remove data after one day.
                                            userMap.put(userName, clients, 86400000, function(result, error) {
                                                if (error) {
                                                    WebSocketHandler.logger.error(
                                                            "Error putting data to clustered userMap for user : "
                                                                    + userName, error);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            // If Vertx is not clustered, use local map.
            var clientMap = sharedData.getLocalMap("clientMap");
            var userName = clientMap.get(clientId);
            clientMap.remove(clientId);
            WebSocketHandler.logger.info("Removing from client map : " + clientId + " : " + userName);
            if (userName) {
                var userMap = sharedData.getLocalMap("userMap");
                var clients = userMap.get(userName);
                if (!clients || clients == null || clients.length == 0)
                    return;

                clients.splice(clients.indexOf(clientId), 1);
                userMap.put(userName, clients);
                WebSocketHandler.logger.info("Removing from user map : " + clientId + " : " + userName);
            }
        }
    },
    getUserFromClientMap : function(clientId, callback) {

    },
    getClientsFromUserMap : function(userName, callback) {
        var sharedData = vertx.sharedData();
        if (vertx.isClustered()) {
            // If Vertx is clustered, use cluster wide maps.
            sharedData.getClusterWideMap("userMap", function(userMap, error) {
                if (error) {
                    WebSocketHandler.logger.error("Error getting clustered userMap for user : " + userName, error);
                    callback(null, error);
                } else {
                    userMap.get(userName, function(clients, error) {
                        if (error) {
                            WebSocketHandler.logger.error(
                                    "Error getting client list from clustered userMap for user : " + userName, error);
                            callback(null, error);
                        } else {
                            WebSocketHandler.logger.info("Client list from clustered userMap : " + clients);
                            callback(clients, null);
                        }
                    });
                }
            });
        } else {
            // If Vertx is not clustered, use local map.
            var userMap = sharedData.getLocalMap("userMap");
            var clients = userMap.get(userName);
            WebSocketHandler.logger.info("Get from user map : " + userName + " : " + clients);
            callback(clients, null);
        }
    }
}