/**
 * Handles operations related to user and authentication.
 */

var UsersHandler = {
    logger : Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.handler.UsersHandler"),
    createUser : function(rc) {		
        var input = rc.getBodyAsJson();
		UsersHandler.logger.info("User input:");
        var user = {};

        // Validate input attributes and populate domain model.
        if (input == null) {
            rc.response().setStatusCode(422).setStatusMessage("Empty request").end();
            return;
        }

        if (input.userName && typeof input.userName === 'string' && input.userName.trim().length > 0) {
            user.userName = input.userName;			
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Mandatory attribute 'userName' is empty.").end();
            return;
        }

        if (input.firstName && typeof input.firstName === 'string' && input.firstName.trim().length > 0) {
            user.firstName = input.firstName;
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Mandatory attribute 'firstName' is empty.").end();
            return;
        }

        if (input.password && typeof input.password === 'string' && input.password.trim().length > 0) {
            user.password = input.password;
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Mandatory attribute 'password' is empty.").end();
            return;
        }
		
		if (input.lastName && typeof input.lastName === 'string' && input.lastName.trim().length > 0) {
            user.lastName = input.lastName;
        } else {
            user.lastName = "";            
        }
		
		user.createdTime = (new Date()).getTime();
		user.lastUpdatedTime = (new Date()).getTime();

		
        vertx.eventBus().send("com.cisco.cmad.so.users.post", user , function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(201).putHeader("location", "/users/" + result.body()).end();
            }
        });
		
    },
    searchUsers : function(rc) {
    	var text = rc.request().getParam("text");
        var skip = rc.request().getParam("skip");
        var limit = rc.request().getParam("limit");

        try {
            if (skip == null || skip.trim().length == 0) {
                skip = 0;
            } else {
                skip = parseInt(skip);
            }
            if (skip < 0)
                throw "Invalid skip value.";

            if (limit == null || limit.trim().length == 0) {
                limit = 100;
            } else {
                limit = parseInt(limit);
            }
            if (limit < 1 || limit > 100)
                throw "Invalid limit value.";

        } catch (e) {
            rc.response().setStatusCode(400).setStatusMessage(
                    "Bad request: skip has to be a positive integer and limit has to be between 1 and 100").end();
            return;
        }

        var searchJson = {
            text : text,
            skip : skip,
            limit : limit
        };

        vertx.eventBus().send("com.cisco.cmad.so.users.search", searchJson, function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(200).write(result.body()).end();
            }
        });
    },
    getUserById : function(rc) {
        var userId = rc.request().getParam("userId");
        if (userId == null || userId.trim().length == 0) {
            rc.response().setStatusCode(404).setStatusMessage("Empty User ID").end();
            return;
        }
        
        var field = rc.request().getParam("type");
        if(field != null && field.trim().toUpperCase() == "USERNAME") 
            field = "userName";
        else
            field = "_id";
        
        var user = {
          userId: userId,
          field: field
        };

        vertx.eventBus().send("com.cisco.cmad.so.users.get", user, function(result, error) {
            if (error) {
                rc.fail(error);
            } else if (result.body() == null) {
                rc.response().setStatusCode(404).setStatusMessage("Not found").end();
            } else {
                rc.response().setStatusCode(200).write(result.body()).end();
            }
        });
    },
    updateUser : function(rc) {
    	var input = rc.getBodyAsJson();
    	var user = {};
    	user.userName = rc.user().principal().sub;
    	var userEntityId = rc.user().principal().id;
    	
    	if (rc.request().getParam("userId") != user.userName && rc.request().getParam("userId") != userEntityId) {
    		rc.response().setStatusCode(403).setStatusMessage("The user being updated is not the same as the authenticated user.").end();
    	}
    	
    	if (input == null) {
            rc.response().setStatusCode(422).setStatusMessage("Empty request").end();
            return;
        }

    	// Make sure that at least one of the editable fields is present in the request.
    	if (!input.userName && !input.firstName && !input.lastName && !input.password) {
    		rc.response().setStatusCode(422).setStatusMessage("No changes to user in request.").end();
            return;
    	}
    	
    	if (input.userName && typeof input.userName === 'string' && input.userName.trim().length > 0) {
            user.userName = input.userName;			
        } 

        if (input.firstName && typeof input.firstName === 'string' && input.firstName.trim().length > 0) {
            user.firstName = input.firstName;
        } 

        if (input.password && typeof input.password === 'string' && input.password.trim().length > 0) {
            user.password = input.password;
        } 
		
		if (input.lastName && typeof input.lastName === 'string' && input.lastName.trim().length > 0) {
            user.lastName = input.lastName;
        } 
		
		user.lastUpdatedUserId = user.userName;
        user.lastUpdatedUserName = rc.user().principal().lastName + ", " + rc.user().principal().firstName;
        user.lastUpdatedTime = (new Date()).getTime();
        
        vertx.eventBus().send("com.cisco.cmad.so.users.update", user, function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(200).end();
            }
        });
        
    },
    generateToken : function(rc) {
		var input = rc.getBodyAsJson();		
		var userName;
		var password;
		var user = {};
		
	    if (input.userName && typeof input.userName === 'string' && input.userName.trim().length > 0) {
            user.userName = input.userName;
			
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Mandatory attribute 'userName' is empty.").end();
            return;
        }

		if (input.password && typeof input.password === 'string' && input.password.trim().length > 0) {
           user.password = input.password;			
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Mandatory attribute 'password' is empty.").end();
            return;
        }		
		
		UsersHandler.logger.info("Sending message to authenticate user"+user);
		
		vertx.eventBus().send("com.cisco.cmad.so.users.authenticate", user , function(result, error) {
            if (error) {
                rc.fail(error);
			} else {
				var respJson = JSON.parse(result.body());				 
				var jwtToken = respJson.jwtToken;
				
				if(jwtToken)
				    rc.response().setStatusCode(201).putHeader("Authorization", jwtToken).write(result.body()).end();                                    
				else
					rc.response().setStatusCode(401).setStatusMessage("Invalid User/password").end();
            }
        });
    }
}