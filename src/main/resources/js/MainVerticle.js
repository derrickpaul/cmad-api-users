/**
 * Entry point for all the server. Here are the key purposes for this Verticle:
 * <br>
 * 1. Define routes for all the APIs. <br>
 * 2. Deploy verticles. <br>
 * 3. Initialize standard handlers like static handler, HTTP body handler, token
 * validator handler, etc. <br>
 * 4. Initialize framework level tasks like logging, exception handling, etc.
 */

var logger = Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.MainVerticle");
logger.info("Initializing main verticle in users micro-service, routers and framework handlers");

// Deploy other verticles
vertx.deployVerticle("com.cisco.cmad.so.verticle.UserVerticle");

// Create router instance.
var Router = require("vertx-web-js/router");
var router = Router.router(vertx);

// Add handler for static content. Used for swagger-ui
var StaticHandler = require("vertx-web-js/static_handler");
router.route("/static/users/*").handler(StaticHandler.create().setWebRoot("web").handle);

// Set a timeout handler to return a timeout HTTP response.
var TimeoutHandler = require("vertx-web-js/timeout_handler");
router.route("/apis/*").handler(TimeoutHandler.create(30000).handle);

// Handle exceptions from any of the REST APIs.
load("classpath:js/handler/ExceptionHandler.js");
router.route("/apis/*").failureHandler(ExceptionHandler.handle);

// Sets chunked response, content type and trace logs.
load("classpath:js/handler/InitializationHandler.js");
router.route("/apis/*").handler(InitializationHandler.handle);

// Add handler to validate JWT token for all but post write operations.
var JWTAuth = require("vertx-auth-jwt-js/jwt_auth");
var JWTAuthHandler = require("vertx-web-js/jwt_auth_handler");

var authConfig = {
  "keyStore" : {
    "type" : "jceks",
    "path" : "keystore.jceks",
    "password" : "secret"
  }
};

var authProvider = JWTAuth.create(vertx, authConfig);

router.route("/apis/*").method("PUT").method("PATCH").method("DELETE").handler(JWTAuthHandler.create(authProvider).handle);

//If anyone hits the base URL, take them to the Swagger UI
router.route("/").handler(
        function(rc) {
            rc.response().setStatusCode(302).setStatusMessage("Redirecting to API documentation...")
                .putHeader("location", "/static/users/swagger-ui/index.html").end();
        });

// Route for handling CORS
router.route("/*").method("OPTIONS").handler(function(rc) {
    rc.response().setStatusCode(200).setStatusMessage("CORS allowed from all domains.")
        .putHeader("Allow", "OPTIONS, GET, HEAD, POST, PUT, DELETE, PATCH")
        .putHeader("Access-Control-Allow-Origin", "*")
        .putHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, PATCH")
        .putHeader("Access-Control-Allow-Headers", "content-type, origin, accept, authorization")
        .putHeader("Access-Control-Expose-Headers", "content-type, location, authorization, transfer-encoding, content-length, cache-control")
        .putHeader("Access-Control-Max-Age", "86400").end();
});

// Add handler to process HTTP body.
var BodyHandler = require("vertx-web-js/body_handler");
router.route("/apis/*").handler(BodyHandler.create().handle);

// Add sub-router which defines handlers for all REST APIs.
load("classpath:js/router/RestApiRouter.js");
router.mountSubRouter("/apis", RestApiRouter.create().router);

//Create and start the server.
//Default port is 8081. Note, UI uses 8080, /questions API uses 8082.
var serverPort = Vertx.currentContext().config().httpPort;
if (serverPort)
 logger.info("Using port from config json");
else
 serverPort = 8084;

var server = vertx.createHttpServer();

// Add Websocket handler for chatting.
load("classpath:js/handler/WebSocketHandler.js");
server.websocketHandler(WebSocketHandler.handle);
server.requestHandler(router.accept).listen(serverPort);
logger.info("Server started on port " + serverPort);