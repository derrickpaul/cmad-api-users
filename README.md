## README
This is a micro-service for the users API as part of the CMAD Stackoverflow use case. 

### Build
#### Maven build
We use maven to build. Running 'mvn clean install' will compile, test and build the project. The fat jar will be placed under the 'target' folder.
#### Docker build
You can create a docker image by running the docker file in the root. In this case you can run the micro-service as a docker container. See the Dockerfile for further details. Note, the Dockerfile exposes port 80.

### Run   
* From the base git folder, run the fat jar as a java application. 
* Option 1: To run with all defaults: 'java -jar target/stackoverflow-api-users-fat.jar'. The application runs on port 8084.
* Option 2: To use the config file (for example, to run on port 8080), run 'java -jar target/stackoverflow-api-users-fat.jar -conf -conf src/main/resources/configuration/config.json'
	* Note, the json should have an attribute named 'httpPort' with the desired port number.

#### MongoDB parameters
* The following MongoDB parameters can be over-ridden by passing the relevant environment variables.  
	* MongoDB database. Environment variable: SO_MONGODB_DATABASE. Default value: cmad
	* MongoDB Connection URL. Environment variable : SO_MONGODB_URL. Default value: mongodb://cmad-mongodb:27017

### API documentation   
* Cloud Reference: http://cmad-apis.derrickpaul.in/static/users/swagger-ui/index.html
* If you have the application running locally, you can use http://localhost:8081/static/users/swagger-ui/index.html to try out the APIs.

### Deploy   
This micro-service is part of the stackoverflow application. Please look into the cmad-overview repository for details on running the complete application - https://bitbucket.org/derrickpaul/cmad-overview/overview
